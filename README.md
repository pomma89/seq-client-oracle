# Seq Client for Oracle

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]

Oracle PL/SQL package which sends log events to a [Seq][seq-website] instance via its REST APIs.

## Table of Contents

- [Install](#install)
  - [1. Setup (as `SYS`)](#1-setup-as-sys)
  - [2. Package deploy](#2-package-deploy)
  - [3. Public synonym (as `SYS`, optional)](#3-public-synonym-as-sys-optional)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

### 1. Setup (as `SYS`)

Following instructions are related to [Setup.sql][seq-client-oracle-setup] file.

Before installing (and customizing) Oracle package, some commands need to be run as `SYS`. Specifically, `SYS` needs to allow remote HTTP calls to Seq instance for a given Oracle user.

[Setup.sql][seq-client-oracle-setup] file contains following parameters:

| Parameter name | Default value | Meaning                                                      |
|----------------|---------------|--------------------------------------------------------------|
| `ORACLE_USER`  |               | Oracle user who needs to send log events to Seq (UPPERCASE). |
| `SEQ_HOST`     |               | Host name on which Seq is listening to.                      |
| `SEQ_PORT`     | 5341          | Port number on which Seq is listening to.                    |

`ORACLE_USER` can be set to `SYS` if you want to share package globally. If so, please remove grants at the beginning of the script and do not skip step 3, which must also be run as `SYS` user.

Before going to step 2, please make sure that there are no networking or security issues blocking HTTP calls from Oracle machine to Seq machine. Using `curl` from Oracle machine, you can easily verify the connectivity with this command (please replace placeholders with proper values):

```shell
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://{SEQ_HOST}:{SEQ_PORT}/api/
```

Result should be similar to the one described in [this Seq documentation page][seq-http-api]:

```http
HTTP/1.1 200 OK
Cache-Control: no-cache
Pragma: no-cache
Transfer-Encoding: chunked
Content-Type: application/json; charset=utf-8
Expires: Wed, 08 Nov 2017 14:07:40 GMT
Server: Microsoft-HTTPAPI/2.0
Date: Thu, 09 Nov 2017 14:07:40 GMT

{"Product":"Seq — structured logs for .NET apps","Version":"4.1.14.0","InstanceName":null,"Links":{"ApiKeysResources":"api/apikeys/resources","AppInstancesResources":"api/appinstances/resources","AppsResources":"api/apps/resources","BackupsResources":"api/backups/resources","DashboardsResources":"api/dashboards/resources","DataResources":"api/data/resources","DiagnosticsResources":"api/diagnostics/resources","EventsResources":"api/events/resources","ExpressionsResources":"api/expressions/resources","FeedsResources":"api/feeds/resources","LicensesResources":"api/licenses/resources","PermalinksResources":"api/permalinks/resources","RetentionPoliciesResources":"api/retentionpolicies/resources","SettingsResources":"api/settings/resources","SignalsResources":"api/signals/resources","SqlQueriesResources":"api/sqlqueries/resources","UpdatesResources":"api/updates/resources","UsersResources":"api/users/resources"}}
```

### 2. Package deploy

Following instructions are related to [Package.sql][seq-client-oracle-package] file.

[Package.sql][seq-client-oracle-package] file contains these parameters:

| Parameter name        | Default value | Meaning                                                           |
|-----------------------|---------------|-------------------------------------------------------------------|
| `ORACLE_USER`         |               | Oracle user for whom package should be created (UPPERCASE).       |
| `ORACLE_PACKAGE`      | seq_log       | Oracle package name for Seq client.                               |
| `SEQ_HOST`            |               | Host name on which Seq is listening to.                           |
| `SEQ_PORT`            | 5341          | Port number on which Seq is listening to.                         |
| `SEQ_DEFAULT_API_KEY` |               | **Default** API KEY which will be used to send log events to Seq. |

`ORACLE_USER` can be set to `SYS` if you want to share package globally. If so, do not skip step 3, which must also be run as `SYS` user.

### 3. Public synonym (as `SYS`, optional)

Following instructions are related to [Synonym.sql][seq-client-oracle-synonym] file.

If you installed Oracle package as `SYS`, then you might want to create a public synonym so that consumers can use it as if it was a system package, without specifying user prefix on each call. This step basically creates a synonym and adds execute grant to a chosen user (not `SYS`, of course).

[Synonym.sql][seq-client-oracle-synonym] file contains these parameters:

| Parameter name   | Default value | Meaning                                                             |
|------------------|---------------|---------------------------------------------------------------------|
| `ORACLE_USER`    |               | Oracle user, not SYS, from whom package should be used (UPPERCASE). |
| `ORACLE_PACKAGE` | seq_log       | Oracle package name for Seq client.                                 |

## Usage

After having installed [Package.sql][seq-client-oracle-package], you can test that everything is working properly by running this command (please replace placeholders with proper values):

```sql
begin
  {ORACLE_USER}.{ORACLE_PACKAGE}.self_test();
end;
```

If test runs OK, then you should find two messages per log level inside your Seq instance, all related to the API KEY specified during package installation; one message is simple, while the other one uses event properties.

If you look for `procedure self_test` inside [Package.sql][seq-client-oracle-package], you will find how each log procedure can be used and how to set event properties.

Events sent through Oracle package will use API KEY specified in `SEQ_DEFAULT_API_KEY` parameter. If some events should use a different API KEY, following procedure can be used (please replace placeholders with proper values):

```sql
begin
  {ORACLE_USER}.{ORACLE_PACKAGE}.set_api_key('{NEW_API_KEY}');
  {ORACLE_USER}.{ORACLE_PACKAGE}.information('Hello Seq');
end;
```

To share a given set of properties among log events, you can set (or unset) a context using `set_context` procedure (please replace placeholders with proper values):

```sql
begin
  {ORACLE_USER}.{ORACLE_PACKAGE}.set_context({EVENT_PROPS});
  {ORACLE_USER}.{ORACLE_PACKAGE}.information('Hello Seq');
  {ORACLE_USER}.{ORACLE_PACKAGE}.set_context(null);
end;
```

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

## License

MIT © 2017-2022 [Alessio Parma][personal-website]

[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pomma89]: https://gitlab.com/pomma89
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[project-license]: https://gitlab.com/pomma89/seq-client-oracle/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[seq-client-oracle-package]: https://gitlab.com/pomma89/seq-client-oracle/blob/main/src/Package.sql
[seq-client-oracle-setup]: https://gitlab.com/pomma89/seq-client-oracle/blob/main/src/Setup.sql
[seq-client-oracle-synonym]: https://gitlab.com/pomma89/seq-client-oracle/blob/main/src/Synonym.sql
[seq-http-api]: https://docs.datalust.co/docs/posting-raw-events
[seq-website]: https://datalust.co/seq
