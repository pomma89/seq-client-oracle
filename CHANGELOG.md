# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.1] - 2020-07-26

### Changed

- Implemented standard CHANGELOG.

## [1.2.0] - 2017-11-25

### Changed

- Added procedure `set_context` to allow sharing properties among log events.

## [1.1.3] - 2017-11-19

### Changed

- Using right property (`@x`) for error stack trace.

## [1.1.2] - 2017-11-15

### Changed

- Made it clear in the docs that `ORACLE_USER` should be UPPERCASE.
- Added an error case to `self_test` procedure.

## [1.1.1] - 2017-11-15

### Changed

- Integer property values are now sent as integers rather than strings.

## [1.1.0] - 2017-11-14

### Changed

- Line number is properly sent as an integer.
- Stack trace is sent when an error is detected.

## [1.0.1] - 2017-11-14

### Changed

- Source context is now set to "anonymous.block" when log is called from an anonymous block.

## [1.0.0] - 2017-11-13

### Changed

- Initial release.

[1.2.1]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.2.0...1.2.1
[1.2.0]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.1.3...v1.2.0
[1.1.3]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/pommalabs/mime-types/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/pommalabs/mime-types/-/tags/v1.0.0
